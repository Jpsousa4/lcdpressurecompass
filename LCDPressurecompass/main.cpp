// IMPORTANT: LCDWIKI_SPI LIBRARY MUST BE SPECIFICALLY
// CONFIGURED FOR EITHER THE TFT SHIELD OR THE BREAKOUT BOARD.

//This program is a demo of how to display picture and 
//how to use rotate function to display string.

//when using the BREAKOUT BOARD only and using these hardware spi lines to the LCD,
//the SDA pin and SCK pin is defined by the system and can't be modified.
//if you don't need to control the LED pin,you can set it to 3.3V and set the pin definition to -1.
//other pins can be defined by youself,for example
//pin usage as follow:
//                 CS  A0/DC  RESET  SDA  SCK  LED    VCC     GND    
//ESP8266 D1 nimi  D1   D3     D2    D7   D5   D4   5V/3.3V   GND

//Remember to set the pins to suit your display module!
/********************************************************************************
* @attention
*
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
* TIME. AS A RESULT, QD electronic SHALL NOT BE HELD LIABLE FOR ANY
* DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
* FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE 
* CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
**********************************************************************************/





#include <Arduino.h>
#include "SparkFunMPU9250-DMP.h"
#include <LCDWIKI_GUI.h> //Core graphics library
#include <LCDWIKI_SPI.h> //Hardware-specific library
#include <math.h>
//#define SerialPort SerialUSB
MPU9250_DMP imu;
//paramters define
#define MODEL SSD1283A
#define CS   4
#define CD   6
//#define SDA  A2   //if you use the hardware spi,this pin is no need to set
//#define SCK  A1   //if you use the hardware spi,this pin is no need to set
#define RST  5
#define LED  -1   //if you don't need to control the LED pin,you should set it to -1 and set it to 3.3V

//the definiens of hardware spi mode as follow:
//if the IC model is known or the modules is unreadable,you can use this constructed function
LCDWIKI_SPI mylcd(MODEL,CS,CD,RST,LED); 

#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

void imusetup()
{
		// Call imu.begin() to verify communication with and
		// initialize the MPU-9250 to it's default values.
		// Most functions return an error code - INV_SUCCESS (0)
		// indicates the IMU was present and successfully set up
		if (imu.begin() != INV_SUCCESS)
		{
			bool ledison = 0;
			while(1)
			{
				pinMode(13,OUTPUT);
				digitalWrite(13,ledison);
				ledison = !ledison;
				delay(100);
			}
		}

		// Use setSensors to turn on or off MPU-9250 sensors.
		// Any of the following defines can be combined:
		// INV_XYZ_GYRO, INV_XYZ_ACCEL, INV_XYZ_COMPASS,
		// INV_X_GYRO, INV_Y_GYRO, or INV_Z_GYRO
		// Enable all sensors:
		imu.setSensors(INV_XYZ_GYRO | INV_XYZ_ACCEL | INV_XYZ_COMPASS);

		// Use setGyroFSR() and setAccelFSR() to configure the
		// gyroscope and accelerometer full scale ranges.
		// Gyro options are +/- 250, 500, 1000, or 2000 dps
		imu.setGyroFSR(2000); // Set gyro to 2000 dps
		// Accel options are +/- 2, 4, 8, or 16 g
		imu.setAccelFSR(2); // Set accel to +/-2g
		// Note: the MPU-9250's magnetometer FSR is set at
		// +/- 4912 uT (micro-tesla's)

		// setLPF() can be used to set the digital low-pass filter
		// of the accelerometer and gyroscope.
		// Can be any of the following: 188, 98, 42, 20, 10, 5
		// (values are in Hz).
		imu.setLPF(5); // Set LPF corner frequency to 5Hz

		// The sample rate of the accel/gyro can be set using
		// setSampleRate. Acceptable values range from 4Hz to 1kHz
		imu.setSampleRate(10); // Set sample rate to 10Hz

		// Likewise, the compass (magnetometer) sample rate can be
		// set using the setCompassSampleRate() function.
		// This value can range between: 1-100Hz
		imu.setCompassSampleRate(10); // Set mag rate to 10Hz
}


void imuloop()
{
	// dataReady() checks to see if new accel/gyro data
	// is available. It will return a boolean true or false
	// (New magnetometer data cannot be checked, as the library
	//  runs that sensor in single-conversion mode.)
	if ( imu.dataReady() )
	{
		// Call update() to update the imu objects sensor data.
		// You can specify which sensors to update by combining
		// UPDATE_ACCEL, UPDATE_GYRO, UPDATE_COMPASS, and/or
		// UPDATE_TEMPERATURE.
		// (The update function defaults to accel, gyro, compass,
		//  so you don't have to specify these values.)
		imu.update(UPDATE_ACCEL | UPDATE_GYRO | UPDATE_COMPASS);
	}
}

void setup() 
{
	imusetup();
  mylcd.Init_LCD();
  mylcd.Set_Text_Size(1);
  mylcd.Fill_Screen(BLACK);
  mylcd.Draw_Circle(65,65,50);
}

float secondsAlive()
{
	  return (float)millis() / 1000;
}

void drawSecondHand(int centerx, int centery, int length)
{
	static int lastCenterX = 0;
	static int lastCenterY = 0;
	static int lastSecondX = 0;
	static int lastSecondY = 0;
	//erase old line before starting a new one
	
	//float secondsAngle = ((float)(secondsAlive()) / 3 * TWO_PI) - HALF_PI;
	
	float magX = imu.calcMag(imu.mx);
	float magY = imu.calcMag(imu.my);
	
	float secondsAngle = imu.computeCompassHeading() / 180.0f * PI; //atan(magY/magX);
	
	int secondX = length * cos(secondsAngle)+centerx;
	int secondY = length * sin(secondsAngle)+centery;
	
	if ((secondX == lastSecondX) && (secondY == lastSecondY)) //if the line hasn't changed, then don't do anything
	{
		return;
	}
	uint16_t currentColor = mylcd.Get_Draw_color();
	mylcd.Set_Draw_color(BLACK);
	mylcd.Draw_Line(lastCenterX,lastCenterY,lastSecondX,lastSecondY);
	mylcd.Set_Draw_color(currentColor);
	mylcd.Draw_Line(centerx,centery,secondX,secondY);
	lastCenterX = centerx;
	lastCenterY = centery;
	lastSecondX = secondX;
	lastSecondY = secondY;	
}

void loop() 
{
  //mylcd.Print_Number_Float(millis()/1000, 0, 24, 0, ' ', 10);
  static unsigned long oneSecondwindow = 0;
  imuloop();
    mylcd.Print_Number_Float((float)imu.calcMag(imu.mx), 2,  0, 0, '.', 0, ' ');
	mylcd.Print_Number_Float((float)imu.calcMag(imu.my), 2,  55, 0, '.', 0, ' ');
	mylcd.Print_Number_Float((float)imu.calcMag(imu.mz), 2,  100, 0, '.', 0, ' ');

  mylcd.Print_Number_Float((float)imu.computeCompassHeading(), 2,  55, 63, '.', 0, ' ');
  if(millis() >= oneSecondwindow)
  {
	  oneSecondwindow += 10;
	  drawSecondHand(65,65,45);
  }

}


